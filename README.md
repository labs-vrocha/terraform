# Versões

- Terraform v1.7.5
- VirtualBox 7.0.14 r161095 (Qt5.15.2)
- MacOS Sonoma 14.2.1 (23C71)

# Como executar o projeto? 

- terraform plan (TF_LOG=DEBUG terraform apply)
- terraform apply (TF_LOG=DEBUG terraform apply)

# Corrigindo o erro Clone *.vdi and *.vmdk to VM folder: exit status 1

- Editar o arquivo /Users/vrocha/Library/VirtualBox/VirtualBox.xml e excluir a referência do disco que não está mais sendo utilizado. 