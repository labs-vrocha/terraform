terraform {
  required_providers {
    docker = {
      source = "terraform-providers/docker"
    }
  }
}

provider "docker" {
  host = "unix:///Users/vrocha/.docker/run/docker.sock"
}

resource "docker_image" "nginx" {
  name = "nginx:latest"
  keep_locally = false
}

resource "docker_image" "httpd" {
  name = "httpd:latest"
  keep_locally = false
}

resource "docker_container" "httpd" {
  image = docker_image.httpd.latest
  name = "httpd-tf"
  ports {
    internal = 80
    external = 8090
  }
}

resource "docker_container" "nginx" {
  image = docker_image.nginx.latest
  name = "nginx-tf"
  ports {
    internal = 80
    external = 8070
  }
}