terraform {
  required_providers {
    virtualbox = {
      source = "terra-farm/virtualbox"
      version = "0.2.2-alpha.1"
    }
  }
}

provider "virtualbox" {

}

resource "virtualbox_vm" "node" {
  count     = 1
  name      = "ubuntu-tf-lab"
  image     = "https://app.vagrantup.com/ubuntu/boxes/trusty64/versions/20190514.0.0/providers/virtualbox/unknown/vagrant.box"
  cpus      = 2
  memory    = "512 mib"
  status = "poweroff"
  network_adapter {
    type = "nat"
  }
}

output "IPAddr" {
  value = element(virtualbox_vm.node.*.network_adapter.0.ipv4_address, 1)
}
